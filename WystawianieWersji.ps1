﻿$source = "D:\Repo\Gomoku"
$release = "D:\WersjaTemp"
$out = "D:\WersjaTemp\log.txt"


function PrzygotujWersje
{
    $tekst = "Trwa kompilacja..."
    echo $tekst
	$outPath = $Global:release    

    $sln = Join-Path -Path $Global:source -ChildPath "Gomoku.sln"

	$msbuild = '"C:\Program Files (x86)\MSBuild\14.0\Bin\MSBuild.exe"'

	$parametry = '/t:Rebuild /p:Configuration=Release /p:Platform="Any CPU"'

    $komenda = "$msbuild $sln $parametry"

	$p = cmd /c $komenda
	$err = $?

    Out-File -FilePath $Global:out -InputObject $p

	if($err -eq $false)
	{        
        echo $p
		Start-Process $sln -Wait
	}         
}


function Testy
{
    $tekst = "Testy"
    echo $tekst
    $xunit = "packages\xunit.runner.console.2.1.0\tools\xunit.console.exe"

    $xunit = Join-Path -Path $source -ChildPath $xunit
    $testy = Join-Path -Path $source -ChildPath "GomokuTests\bin\Release\GomokuTests.dll"
    
    $wynik = cmd /c $xunit $testy 
    $err = $?

    Out-File -FilePath $Global:out -InputObject $wynik -Append 

    if ($err -eq $false)
    {
        echo "Coś poszło nie tak"
    }
}


function Kopiuj
{
    $tekst = "Kopiowanie gotowych bibliotek"
    echo $tekst

    $sciezka = Join-Path -Path $Global:source -ChildPath "Gomoku\bin\Release"
    $ex = @('*.pdb')
    $pliki = Get-ChildItem $sciezka -Exclude $ex 
    
    ForEach ($item in $pliki)
    {
        if (Test-Path $item)
        {
            Copy-Item $item $Global:release
        }
    } 
}

function Inicjalizacja
{ 
    if (!(Test-Path $Global:release))
    {
        $null = New-Item -Path $Global:release -ItemType Directory
    }
    else
    {
        $null = Remove-Item $Global:release -Recurse -Force
        $null = New-Item -Path $Global:release -ItemType Directory
    }
}

Inicjalizacja
PrzygotujWersje
Testy
Kopiuj
