﻿using Xunit;
using Shouldly;
using Gomoku.Models.Enums;
using GomokuTests;

namespace Gomoku.Core.Tests
{
	public class PlanszaServiceTests
	{

		[Fact]
		public async void poczatkowo_plansza_pusta()
		{
			await ThreadHelper.StartSTATask(() =>
			{
				PlanszaService plansza = new PlanszaService();

				bool wynik = SprawdzCzyPlanszaPusta(plansza);

				wynik.ShouldBe(true);
			});
		}

		[Fact]
		public async void ustawianie_pionka_nie_nadpisuje_starych_pionkow()
		{
			await ThreadHelper.StartSTATask(() =>
			{
				PlanszaService plansza = new PlanszaService();

				plansza.UstawPionekNaPolu(0, 0, Kolor.Bialy);
				plansza.UstawPionekNaPolu(0, 0, Kolor.Czarny);

				plansza.Plansza[0, 0].Kolor.ShouldBe(Kolor.Bialy);
			});
		}

		[Fact]
		public async void ustawianie_pionka_zwraca_true_jezeli_ustawi_pionek()
		{
			await ThreadHelper.StartSTATask(() =>
			{
				PlanszaService plansza = new PlanszaService();

				var wyn = plansza.UstawPionekNaPolu(0, 0, Kolor.Bialy);

				wyn.ShouldBeTrue();
			});
		}

		[Fact]
		public async void ustawianie_pionka_zwraca_false_jezeli_nie_ustawi_pionka()
		{
			await ThreadHelper.StartSTATask(() =>
			{
				PlanszaService plansza = new PlanszaService();

				plansza.UstawPionekNaPolu(0, 0, Kolor.Bialy);
				var wyn = plansza.UstawPionekNaPolu(0, 0, Kolor.Bialy);

				wyn.ShouldBeFalse();
			});
		}

		[Fact]
		public async void ustawianie_pionka_nie_pozwala_na_ustawienie_pionka_bez_koloru()
		{
			await ThreadHelper.StartSTATask(() =>
			{
				PlanszaService plansza = new PlanszaService();

				bool wyn = plansza.UstawPionekNaPolu(0, 0, Kolor.Brak);

				wyn.ShouldBeFalse();
			});
		}

		[Theory]
		[InlineData(Kolor.Bialy)]
		[InlineData(Kolor.Czarny)]
		public async void ustawianie_pionka_ustawia_podany_kolor(Kolor kolor)
		{
			await ThreadHelper.StartSTATask(() =>
			{
				PlanszaService plansza = new PlanszaService();

				plansza.UstawPionekNaPolu(0, 0, kolor);

				plansza.Plansza[0, 0].Kolor.ShouldBe(kolor);
			});
		}

		[Theory]
		[InlineData(1, 2)]
		[InlineData(4, 1)]
		[InlineData(1, 0)]
		[InlineData(5, 10)]
		[InlineData(11, 14)]
		public async void ustawianie_pionka_ustawia_go_na_wlasciwych_wspolzednych(int x, int y)
		{
			await ThreadHelper.StartSTATask(() =>
			{
				PlanszaService plansza = new PlanszaService();

				plansza.UstawPionekNaPolu(x, y, Kolor.Bialy);

				plansza.Plansza[x, y].Kolor.ShouldBe(Kolor.Bialy);
			});
		}


		[Fact]
		public async void czyszczenie_planszy_usuwa_ustawione_pionki()
		{
			await ThreadHelper.StartSTATask(() =>
			{
				PlanszaService plansza = new PlanszaService();

				plansza.UstawPionekNaPolu(0, 0, Kolor.Bialy);
				plansza.UstawPionekNaPolu(5, 6, Kolor.Bialy);
				plansza.UstawPionekNaPolu(4, 7, Kolor.Czarny);
				plansza.UstawPionekNaPolu(11, 2, Kolor.Czarny);

				plansza.WyczyscPlansze();

				var wyn = SprawdzCzyPlanszaPusta(plansza);

				wyn.ShouldBeTrue();
			});
		}

		[Theory]
		[InlineData(Kolor.Bialy)]
		[InlineData(Kolor.Czarny)]
		public async void sprawdzanie_wygranej_w_kolumnie(Kolor kolor)
		{
			await ThreadHelper.StartSTATask(() =>
			{
				PlanszaService plansza = new PlanszaService();

				plansza.UstawPionekNaPolu(5, 3, Kolor.Czarny);
				plansza.UstawPionekNaPolu(5, 4, Kolor.Czarny);
				plansza.UstawPionekNaPolu(5, 5, Kolor.Czarny);
				plansza.UstawPionekNaPolu(5, 6, Kolor.Czarny);
				plansza.UstawPionekNaPolu(5, 7, Kolor.Czarny);

				plansza.UstawPionekNaPolu(12, 2, Kolor.Bialy);
				plansza.UstawPionekNaPolu(12, 4, Kolor.Bialy);
				plansza.UstawPionekNaPolu(12, 5, Kolor.Bialy);
				plansza.UstawPionekNaPolu(12, 6, Kolor.Bialy);
				plansza.UstawPionekNaPolu(12, 7, Kolor.Bialy);

				if (kolor == Kolor.Bialy)
				{
					plansza.SprawdzCzyZwyciezyl(kolor).ShouldBeFalse();
				}
				else
				{
					plansza.SprawdzCzyZwyciezyl(kolor).ShouldBeTrue();
				}
			});
		}

		[Theory]
		[InlineData(Kolor.Bialy)]
		[InlineData(Kolor.Czarny)]
		public async void sprawdzanie_wygranej_we_wierszu(Kolor kolor)
		{
			await ThreadHelper.StartSTATask(() =>
			{
				PlanszaService plansza = new PlanszaService();

				plansza.UstawPionekNaPolu(5, 1, Kolor.Czarny);
				plansza.UstawPionekNaPolu(6, 1, Kolor.Czarny);
				plansza.UstawPionekNaPolu(7, 1, Kolor.Czarny);
				plansza.UstawPionekNaPolu(8, 1, Kolor.Czarny);
				plansza.UstawPionekNaPolu(10, 1, Kolor.Czarny);

				plansza.UstawPionekNaPolu(8, 3, Kolor.Bialy);
				plansza.UstawPionekNaPolu(9, 3, Kolor.Bialy);
				plansza.UstawPionekNaPolu(10, 3, Kolor.Bialy);
				plansza.UstawPionekNaPolu(11, 3, Kolor.Bialy);
				plansza.UstawPionekNaPolu(12, 3, Kolor.Bialy);

				if (kolor == Kolor.Czarny)
				{
					plansza.SprawdzCzyZwyciezyl(kolor).ShouldBeFalse();
				}
				else
				{
					plansza.SprawdzCzyZwyciezyl(kolor).ShouldBeTrue();
				}
			});
		}


		[Theory]
		[InlineData(Kolor.Bialy)]
		[InlineData(Kolor.Czarny)]
		public async void sprawdzanie_wygranej_po_skosie_w_prawo(Kolor kolor)
		{
			await ThreadHelper.StartSTATask(() =>
			{
				PlanszaService plansza = new PlanszaService();

				plansza.UstawPionekNaPolu(5, 3, Kolor.Czarny);
				plansza.UstawPionekNaPolu(6, 4, Kolor.Czarny);
				plansza.UstawPionekNaPolu(7, 5, Kolor.Czarny);
				plansza.UstawPionekNaPolu(8, 6, Kolor.Czarny);
				plansza.UstawPionekNaPolu(9, 7, Kolor.Czarny);

				plansza.UstawPionekNaPolu(1, 3, Kolor.Bialy);
				plansza.UstawPionekNaPolu(2, 4, Kolor.Bialy);
				plansza.UstawPionekNaPolu(2, 5, Kolor.Bialy);
				plansza.UstawPionekNaPolu(4, 6, Kolor.Bialy);
				plansza.UstawPionekNaPolu(5, 7, Kolor.Bialy);

				if (kolor == Kolor.Bialy)
				{
					plansza.SprawdzCzyZwyciezyl(kolor).ShouldBeFalse();
				}
				else
				{
					plansza.SprawdzCzyZwyciezyl(kolor).ShouldBeTrue();
				}
			});
		}

		[Theory]
		[InlineData(Kolor.Bialy)]
		[InlineData(Kolor.Czarny)]
		public async void sprawdzanie_wygranej_po_skosie_w_lewo(Kolor kolor)
		{
			await ThreadHelper.StartSTATask(() =>
			{
				PlanszaService plansza = new PlanszaService();

				plansza.UstawPionekNaPolu(10, 3, Kolor.Czarny);
				plansza.UstawPionekNaPolu(9, 4, Kolor.Czarny);
				plansza.UstawPionekNaPolu(8, 5, Kolor.Czarny);
				plansza.UstawPionekNaPolu(7, 6, Kolor.Czarny);
				plansza.UstawPionekNaPolu(6, 6, Kolor.Czarny);

				plansza.UstawPionekNaPolu(4, 3, Kolor.Bialy);
				plansza.UstawPionekNaPolu(3, 4, Kolor.Bialy);
				plansza.UstawPionekNaPolu(2, 5, Kolor.Bialy);
				plansza.UstawPionekNaPolu(1, 6, Kolor.Bialy);
				plansza.UstawPionekNaPolu(0, 7, Kolor.Bialy);

				if (kolor == Kolor.Czarny)
				{
					plansza.SprawdzCzyZwyciezyl(kolor).ShouldBeFalse();
				}
				else
				{
					plansza.SprawdzCzyZwyciezyl(kolor).ShouldBeTrue();
				}
			});
		}


		[Fact]
		public async void zwroc_elementy_zwraca_wlasciwa_liczbe_obiektow()
		{
			await ThreadHelper.StartSTATask(() =>
			{
				PlanszaService plansza = new PlanszaService();

				var lista = plansza.ZwrocElementyInterfaceu();
				var ilosc = plansza.Plansza.GetLength(0) * plansza.Plansza.GetLength(1) * 2;

				lista.Count.ShouldBe(ilosc);
			});
		}

		private static bool SprawdzCzyPlanszaPusta(PlanszaService plansza)
		{
			bool wynik = true;
			foreach (var item in plansza.Plansza)
			{
				wynik &= item.Kolor == Kolor.Brak;
			}

			return wynik;
		}
	}
}