﻿using System;
using System.Linq;
using Xunit;
using Shouldly;
using Gomoku.Models.Enums;
using NSubstitute;
using Gomoku.Core.Interfaces;
using System.Collections.Generic;

namespace Gomoku.Core.Tests
{
	public class AIServiceTests
	{
		[Fact]
		public void ustawStartowePionki_ustawia_dokladnie_3_pionki()
		{
			var planszaMock = Substitute.For<IPlanszaService>();
			planszaMock.UstawPionekNaPolu(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<Kolor>()).Returns(true);
			AIService ai = new AIService(planszaMock, new Random());

			ai.UstawStartowePionki();

			planszaMock.ReceivedCalls().Count().ShouldBe(3);
		}

		[Fact]
		public void ustawStartowePionki_ustawia_dokladnie_3_pionki_mimo_zajetych_pol()
		{
			var planszaMock = Substitute.For<IPlanszaService>();
			planszaMock.UstawPionekNaPolu(Arg.Any<int>(), Arg.Any<int>(), Kolor.Bialy).Returns(false, true);
			planszaMock.UstawPionekNaPolu(Arg.Any<int>(), Arg.Any<int>(), Kolor.Czarny).Returns(false, false, true);
			AIService ai = new AIService(planszaMock, new Random());

			ai.UstawStartowePionki();

			planszaMock.ReceivedCalls().Count().ShouldBe(6);
		}

		[Fact]
		public void ustawStartowePionki_ustawia_dokladnie_2_czarne()
		{
			var planszaMock = Substitute.For<IPlanszaService>();
			planszaMock.UstawPionekNaPolu(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<Kolor>()).Returns(true);
			AIService ai = new AIService(planszaMock, new Random());

			ai.UstawStartowePionki();

			planszaMock.ReceivedCalls().Where(i => (Kolor)i.GetArguments()[2] == Kolor.Czarny).Count().ShouldBe(2);
		}

		[Fact]
		public void ustawStartowePionki_ustawia_dokladnie_1_bialy()
		{
			var planszaMock = Substitute.For<IPlanszaService>();
			planszaMock.UstawPionekNaPolu(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<Kolor>()).Returns(true);
			AIService ai = new AIService(planszaMock, new Random());

			ai.UstawStartowePionki();

			planszaMock.ReceivedCalls().Where(i => (Kolor)i.GetArguments()[2] == Kolor.Bialy).Count().ShouldBe(1);
		}

		[Fact]
		public void wybierz_kolor_nie_wybiera_nieistniejacego_koloru()
		{
			var planszaMock = Substitute.For<IPlanszaService>();
			AIService ai = new AIService(planszaMock, new Random());

			for (int i = 0; i < 100; i++)
			{
				ai.WybierzKolor().ShouldBeOneOf(Kolor.Bialy, Kolor.Czarny);
			}
		}

		[Fact]
		public void wybierz_kolor_nie_wybiera_tego_samego_koloru()
		{
			var planszaMock = Substitute.For<IPlanszaService>();
			AIService ai = new AIService(planszaMock, new Random());

			List<Kolor> wybory = new List<Kolor>();

			for (int i = 0; i < 100; i++)
			{
				wybory.Add(ai.WybierzKolor());
			}

			wybory.Where(k => k == Kolor.Czarny).Count().ShouldBeInRange(1, 99);
		}

		[Theory]
		[InlineData(Kolor.Bialy)]
		[InlineData(Kolor.Czarny)]
		public void wykonaj_ruch_stawia_dokladnie_jeden_pionek(Kolor kolor)
		{
			var planszaMock = Substitute.For<IPlanszaService>();
			planszaMock.UstawPionekNaPolu(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<Kolor>()).Returns(true);
			AIService ai = new AIService(planszaMock, new Random());

			ai.WykonajRuch(kolor);

			planszaMock.ReceivedCalls().Count().ShouldBe(1);
		}

		[Fact]
		public void wykonaj_ruch_stawia_pionek_nie_wykraczajac_poza_tablice()
		{
			var planszaMock = Substitute.For<IPlanszaService>();
			planszaMock.UstawPionekNaPolu(Arg.Any<int>(), Arg.Any<int>(), Arg.Any<Kolor>()).Returns(true);
			AIService ai = new AIService(planszaMock, new Random());

			for (int i = 0; i < 100; i++)
			{
				ai.WykonajRuch(Kolor.Bialy);
			}

			planszaMock.ReceivedCalls()
				.Select(c => new { X = (int)c.GetArguments()[0], Y = (int)c.GetArguments()[1] })
				.Where(o => o.X < 0 || o.X > Ustawienia.WielkoscPlanszy - 1 || o.Y < 0 || o.Y > Ustawienia.WielkoscPlanszy - 1)
				.Count()
				.ShouldBe(0);
		}

	}
}