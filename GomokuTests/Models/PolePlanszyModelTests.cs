﻿using Shouldly;
using Xunit;
using System.Windows.Media;
using GomokuTests;
using Gomoku.Models.Enums;
using System;

namespace Gomoku.Models.Tests
{
	public class PolePlanszyModelTests
	{
		[Fact]
		public async void zmiana_koloru_zmienia_fill_pola_pionka()
		{
			await ThreadHelper.StartSTATask(() =>
			{
				PolePlanszyModel pole = new PolePlanszyModel(0, 0);

				pole.Kolor = Kolor.Bialy;
				var fill1 = pole.WidokPionka.Fill;
				pole.Kolor = Kolor.Czarny;
				var fill2 = pole.WidokPionka.Fill;

				fill1.ShouldNotBe(fill2);
			});
		}

		[Fact]
		public async void pionek_domyslnie_jest_pusty()
		{
			await ThreadHelper.StartSTATask(() =>
			{
				PolePlanszyModel pole = new PolePlanszyModel(0, 0);
				
				pole.Kolor.ShouldBe(Kolor.Brak);
			});
		}

		[Theory]
		[InlineData(Kolor.Brak)]
		[InlineData(Kolor.Bialy)]
		[InlineData(Kolor.Czarny)]
		public async void kolor_pionka_odpowiada_kolorowi_w_modelu(Kolor kolor)
		{
			await ThreadHelper.StartSTATask(() =>
			{
				PolePlanszyModel pole = new PolePlanszyModel(0, 0);

				pole.Kolor = kolor;

				switch (kolor)
				{
					case Kolor.Brak:
						pole.WidokPionka.Fill.ShouldBe(Brushes.Transparent);
						break;
					case Kolor.Bialy:
						pole.WidokPionka.Fill.ShouldBe(Brushes.White);
						break;
					case Kolor.Czarny:
						pole.WidokPionka.Fill.ShouldBe(Brushes.Black);
						break;
					default:
						throw new Exception("Nie obslugiwany kolor");
				}
				
			});
		}

	}
}