﻿using Gomoku.Core;
using Gomoku.Core.Interfaces;
using Gomoku.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Gomoku
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private readonly IEngine gra;

		public MainWindow()
		{
			InitializeComponent();
			gra = new GameEngine(OdkryjWyborKoloru, WyswietlWynik);

			grWybor.Visibility = Visibility.Hidden;

			gra.ZwrocElementyInterfaceu().ForEach(u => canPlansza.Children.Add(u));
			canPlansza.MouseLeftButtonDown += gra.OnMouseClick;
		}

		private void WyswietlWynik(string komunikat)
		{
			if (!tbkWygrana.Dispatcher.CheckAccess())
			{
				tbkWygrana.Dispatcher.Invoke(() => WyswietlWynik(komunikat));
			}
			else
			{
				tbkWygrana.Text = komunikat;
				tbkWygrana.Visibility = Visibility.Visible;
			}
		}

		private void btNowaGra_Click(object sender, RoutedEventArgs e)
		{
			if (gra.CzyGraWToku())
			{
				if (MessageBox.Show("Czy napewno chcesz przerwać bierzącą gre i zacząć od nowa?", "Gomoku", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
				{
					return;
				}
			}

			tbkWygrana.Visibility = Visibility.Hidden;
			grWybor.Visibility = Visibility.Hidden;
			gra.RozpocznijGre();

		}

		void OdkryjWyborKoloru()
		{
			if (!grWybor.Dispatcher.CheckAccess())
			{
				grWybor.Dispatcher.Invoke(() => OdkryjWyborKoloru());
			}
			else
			{
				grWybor.Visibility = Visibility.Visible;
			}
		}

		private void btBialy_Click(object sender, RoutedEventArgs e)
		{
			grWybor.Visibility = Visibility.Hidden;
			gra.WybiezKolor(Kolor.Bialy);
		}

		private void btCzarny_Click(object sender, RoutedEventArgs e)
		{
			grWybor.Visibility = Visibility.Hidden;
			gra.WybiezKolor(Kolor.Czarny);
		}
	}
}
