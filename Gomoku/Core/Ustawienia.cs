﻿namespace Gomoku.Core
{
	public static class Ustawienia
	{
		public static int Skala { get; set; }
		public static int WielkoscPlanszy { get; set; }

		static Ustawienia()
		{
			Skala = 50;
			WielkoscPlanszy = 15;
		}
	}
}
