﻿using System.Collections.Generic;
using Gomoku.Models.Enums;
using Gomoku.Core.Interfaces;
using System.Windows;
using Gomoku.Models;

namespace Gomoku.Core
{
	public class PlanszaService : IPlanszaService
	{
		public PolePlanszyModel[,] Plansza { get; private set; }

		public PlanszaService()
		{
			Plansza = new PolePlanszyModel[Ustawienia.WielkoscPlanszy, Ustawienia.WielkoscPlanszy];
			for (int i = 0; i < Ustawienia.WielkoscPlanszy; i++)
			{
				for (int j = 0; j < Ustawienia.WielkoscPlanszy; j++)
				{
					Plansza[i, j] = new PolePlanszyModel(i, j);
				}
			}

			WyczyscPlansze();
		}

		public void WyczyscPlansze()
		{
			for (int i = 0; i < Ustawienia.WielkoscPlanszy; i++)
			{
				for (int j = 0; j < Ustawienia.WielkoscPlanszy; j++)
				{
					Plansza[i, j].Kolor = Kolor.Brak;
				}
			}
		}

		public bool SprawdzCzyZwyciezyl(Kolor kolor)
		{
			bool wynik = SprawdzWiersze(kolor);
			wynik |= SprawdzKolumny(kolor);
			wynik |= SprawdzSkosLewo(kolor);
			wynik |= SprawdzSkosPrawo(kolor);
			return wynik;
		}

		private bool SprawdzSkosPrawo(Kolor kolor)
		{
			int licznik = 0;
			for (int x = 14; x > 3; x--)
			{
				int y = 0;

				for (int x1 = x, y1 = y; x1 >= 0 && y1 < 15; x1--, y1++)
				{
					if (Plansza[x1, y1].Kolor == kolor)
					{
						licznik++;
					}
					else
					{
						licznik = 0;
					}

					if (licznik == 5)
					{
						return true;
					}
				}
				licznik = 0;
			}

			for (int y = 1; y < 11; y++)
			{
				int x = 14;

				for (int x1 = x, y1 = y; x1 >= 0 && y1 < 15; x1--, y1++)
				{
					if (Plansza[x1, y1].Kolor == kolor)
					{
						licznik++;
					}
					else
					{
						licznik = 0;
					}

					if (licznik == 5)
					{
						return true;
					}
				}
				licznik = 0;
			}

			return false;
		}

		private bool SprawdzSkosLewo(Kolor kolor)
		{
			int licznik = 0;
			for (int x = 0; x < 11; x++)
			{
				int y = 0;

				for (int x1 = x, y1 = y; x1 < 15 && y1 < 15; x1++, y1++)
				{
					if (Plansza[x1, y1].Kolor == kolor)
					{
						licznik++;
					}
					else
					{
						licznik = 0;
					}

					if (licznik == 5)
					{
						return true;
					}
				}
				licznik = 0;
			}

			for (int y = 1; y < 11; y++)
			{
				int x = 0;

				for (int x1 = x, y1 = y; x1 < 15 && y1 < 15; x1++, y1++)
				{
					if (Plansza[x1, y1].Kolor == kolor)
					{
						licznik++;
					}
					else
					{
						licznik = 0;
					}

					if (licznik == 5)
					{
						return true;
					}
				}
				licznik = 0;
			}

			return false;
		}

		private bool SprawdzKolumny(Kolor kolor)
		{
			int licznik = 0;
			for (int x = 0; x < 15; x++)
			{
				for (int y = 0; y < 15; y++)
				{
					if (Plansza[x, y].Kolor == kolor)
					{
						licznik++;
					}
					else
					{
						licznik = 0;
					}

					if (licznik == 5)
					{
						return true;
					}
				}

				licznik = 0;
			}
			return false;
		}

		private bool SprawdzWiersze(Kolor kolor)
		{
			int licznik = 0;
			for (int y = 0; y < 15; y++)
			{
				for (int x = 0; x < 15; x++)
				{
					if (Plansza[x, y].Kolor == kolor)
					{
						licznik++;
					}
					else
					{
						licznik = 0;
					}

					if (licznik == 5)
					{
						return true;
					}
				}

				licznik = 0;
			}
			return false;
		}

		public bool UstawPionekNaPolu(int x, int y, Kolor kolor)
		{
			if (Plansza[x, y].Kolor == Kolor.Brak && kolor != Kolor.Brak)
			{
				Plansza[x, y].Kolor = kolor;
				return true;
			}
			else
			{
				return false;
			}
		}

		public List<UIElement> ZwrocElementyInterfaceu()
		{
			List<UIElement> lista = new List<UIElement>();
			foreach (var item in Plansza)
			{
				lista.Add(item.WidokPionka);
				lista.Add(item.WidokPola);
			}
			return lista;
		}
	}
}
