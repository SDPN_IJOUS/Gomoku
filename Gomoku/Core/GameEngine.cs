﻿using Gomoku.Core.Interfaces;
using Gomoku.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows;

namespace Gomoku.Core
{

	public class GameEngine : IEngine
	{
		public Gracz AktywnyGracz { get; set; }
		public Gracz NieaktywnyGracz()
		{
			return (Gracz)(((int)AktywnyGracz + 1) % 2);
		}

		public Dictionary<Gracz, Kolor> KoloryGraczy { get; set; }

		private Stadium StanGry;
		private readonly Random rng;
		private readonly BackgroundWorker worker;
		private readonly IAIService ai;
		private readonly IPlanszaService plansza;
		private readonly Action<string> wyswietlWynik;

		private readonly BeginEngine poczatek;
		private readonly MidgameEngine srodek;

		private bool restart;

		public GameEngine(Action odkryjWybor, Action<string> wyswietlWynik)
		{
			rng = new Random();
			plansza = new PlanszaService();
			ai = new AIService(plansza, rng);
			this.wyswietlWynik = wyswietlWynik;

			KoloryGraczy = new Dictionary<Gracz, Kolor>();

			poczatek = new BeginEngine(ai, plansza, rng, odkryjWybor);
			srodek = new MidgameEngine(ai, plansza, rng, KoloryGraczy);

			worker = new BackgroundWorker() { WorkerSupportsCancellation = true };
			worker.DoWork += Worker_DoWork;
			worker.RunWorkerCompleted += Worker_RunWorkerCompleted;
			restart = false;
		}

		private void Worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if (e.Cancelled && restart)
			{
				restart = false;
				worker.RunWorkerAsync();
			}
		}

		private void Worker_DoWork(object sender, DoWorkEventArgs e)
		{
			BackgroundWorker bw = sender as BackgroundWorker;

			bool koniec = false;
			StanGry = Stadium.Poczatek;
			plansza.WyczyscPlansze();
			poczatek.Reset();

			do
			{
				switch (StanGry)
				{
					case Stadium.Poczatek:
						poczatek.Graj(KoloryGraczy);

						if (bw.CancellationPending)	continue;

						AktywnyGracz = KoloryGraczy.First(g => g.Value == Kolor.Bialy).Key;
						StanGry = Stadium.GraWlasciwa;
						break;
					case Stadium.GraWlasciwa:
						srodek.Graj(AktywnyGracz);

						if (plansza.SprawdzCzyZwyciezyl(KoloryGraczy[AktywnyGracz]))
						{
							StanGry = Stadium.Wyniki;
						}
						else
						{
							AktywnyGracz = NieaktywnyGracz();
						}
						break;
					case Stadium.Wyniki:
						WyswietlenieZwyciezcy();
						koniec = true;
						break;
				}
			} while (!koniec && !bw.CancellationPending);

			if (bw.CancellationPending)
			{
				e.Cancel = true;
			}

		}

		public bool CzyGraWToku()
		{
			return worker.IsBusy;
		}

		public void RozpocznijGre()
		{
			if (worker.IsBusy)
			{
				restart = true;
				poczatek.CancelWait();
				srodek.CancelWait();
				worker.CancelAsync();
			}
			else
			{
				worker.RunWorkerAsync();
			}
		}

		public void OnMouseClick(object sender, MouseButtonEventArgs e)
		{
			if (StanGry == Stadium.Poczatek)
			{
				poczatek.OnMouseClick(sender, e);
			}
			else if (StanGry == Stadium.GraWlasciwa)
			{
				srodek.OnMouseClick(sender, e);
			}

		}

		public void WybiezKolor(Kolor kolor)
		{
			if (StanGry == Stadium.Poczatek)
			{
				poczatek.WybierzKolor(kolor);
			}
		}

		public List<UIElement> ZwrocElementyInterfaceu()
		{
			return plansza.ZwrocElementyInterfaceu();
		}

		private void WyswietlenieZwyciezcy()
		{
			if (AktywnyGracz == Gracz.Czlowiek)
			{
				wyswietlWynik("Wygrałeś!");
			}
			else
			{
				wyswietlWynik("Przegrałeś!");
			}
		}
	}
}
