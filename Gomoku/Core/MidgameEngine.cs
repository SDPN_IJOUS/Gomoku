﻿using Gomoku.Core.Interfaces;
using Gomoku.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Input;

namespace Gomoku.Core
{
	class MidgameEngine
	{

		private ManualResetEvent ruch;
		private readonly Random rng;
		private readonly IAIService ai;
		private readonly IPlanszaService plansza;
		private Dictionary<Gracz, Kolor> koloryGraczy;

		private bool odblokowaneUI;

		public MidgameEngine(IAIService ai, IPlanszaService plansza, Random rng, Dictionary<Gracz, Kolor> koloryGraczy)
		{
			this.rng = rng;
			this.ai = ai;
			this.plansza = plansza;
			this.koloryGraczy = koloryGraczy;

			ruch = new ManualResetEvent(false);

			odblokowaneUI = false;
		}

		public void Graj(Gracz aktywnyGracz)
		{
			if (aktywnyGracz == Gracz.Czlowiek)
			{
				ruch.Reset();
				odblokowaneUI = true;
				ruch.WaitOne();
				odblokowaneUI = false;
			}
			else
			{
				Thread.Sleep(200);
				ai.WykonajRuch(koloryGraczy[aktywnyGracz]);
				Thread.Sleep(200);
			}

		}

		internal void OnMouseClick(object sender, MouseButtonEventArgs e)
		{
			if (odblokowaneUI)
			{
				Canvas can = sender as Canvas;
				var poz = e.MouseDevice.GetPosition(can);

				bool test = plansza.UstawPionekNaPolu((int)Math.Floor(poz.X / Ustawienia.Skala), (int)Math.Floor(poz.Y / Ustawienia.Skala), koloryGraczy[Gracz.Czlowiek]);

				if (test)
				{
					ruch.Set();
				}
			}
		}

		internal void CancelWait()
		{
			ruch.Set();
		}
	}
}
