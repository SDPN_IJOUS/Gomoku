﻿using Gomoku.Core.Interfaces;
using Gomoku.Models.Enums;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Input;

namespace Gomoku.Core
{
	public class BeginEngine
	{
		private readonly ManualResetEvent[] konce;
		private readonly Random rng;
		private readonly IAIService ai;
		private readonly IPlanszaService plansza;
		private readonly Action odkryjWybor;

		private Kolor wybranyKolor;

		private bool odblokowaneUI;
		private int licznikKlikniec;

		public BeginEngine(IAIService ai, IPlanszaService plansza, Random rng, Action odkryjWybor)
		{
			konce = new ManualResetEvent[3];
			for (int i = 0; i < 3; i++)
			{
				konce[i] = new ManualResetEvent(false);
			}

			this.rng = rng;
			this.ai = ai;
			this.plansza = plansza;
			this.odkryjWybor = odkryjWybor;

			odblokowaneUI = false;
			licznikKlikniec = 0;
			wybranyKolor = Kolor.Brak;
		}

		public void Graj(Dictionary<Gracz, Kolor> koloryGraczy)
		{
			Gracz aktywnyGracz = (Gracz)rng.Next(0, 2);
			Gracz nieaktywnyGracz = (Gracz)(((int)aktywnyGracz + 1) % 2);

			if (aktywnyGracz == Gracz.Czlowiek)
			{
				odblokowaneUI = true;

				foreach (var watek in konce)
				{
					watek.WaitOne();
				}
				odblokowaneUI = false;

				koloryGraczy[nieaktywnyGracz] = ai.WybierzKolor();
			}
			else
			{
				ai.UstawStartowePionki();
				odkryjWybor();

				konce[0].WaitOne();
				koloryGraczy[nieaktywnyGracz] = wybranyKolor;
			}

			if (koloryGraczy[nieaktywnyGracz] == Kolor.Bialy)
			{
				koloryGraczy[aktywnyGracz] = Kolor.Czarny;
			}
			else
			{
				koloryGraczy[aktywnyGracz] = Kolor.Bialy;
			}
		}

		internal void OnMouseClick(object sender, MouseButtonEventArgs e)
		{
			if (odblokowaneUI)
			{
				Canvas can = sender as Canvas;
				var poz = e.MouseDevice.GetPosition(can);
				if (licznikKlikniec < 2)
				{
					plansza.UstawPionekNaPolu((int)Math.Floor(poz.X / Ustawienia.Skala), (int)Math.Floor(poz.Y / Ustawienia.Skala), Kolor.Czarny);
				}
				else
				{
					plansza.UstawPionekNaPolu((int)Math.Floor(poz.X / Ustawienia.Skala), (int)Math.Floor(poz.Y / Ustawienia.Skala), Kolor.Bialy);
				}

				konce[licznikKlikniec].Set();
				licznikKlikniec++;
			}
		}

		internal void Reset()
		{
			foreach (var item in konce)
			{
				item.Reset();
			}
			licznikKlikniec = 0;
		}

		internal void WybierzKolor(Kolor kolor)
		{
			wybranyKolor = kolor;
			konce[0].Set();
		}

		internal void CancelWait()
		{
			foreach (var item in konce)
			{
				item.Set();
			}
		}
	}
}
