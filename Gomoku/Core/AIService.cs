﻿using Gomoku.Core.Interfaces;
using System;
using Gomoku.Models.Enums;
using System.Threading;

namespace Gomoku.Core
{
	public class AIService : IAIService
	{
		private readonly IPlanszaService plansza;
		private readonly Random rng;

		public AIService(IPlanszaService plansza, Random rng)
		{
			this.plansza = plansza;
			this.rng = rng;
		}

		public void UstawStartowePionki()
		{
			int i = 0;
			do
			{
				bool wynik;
				if (i < 2)
				{
					wynik = LosujPionek(Kolor.Czarny);
				}
				else
				{
					wynik = LosujPionek(Kolor.Bialy);
				}

				if (wynik)
				{
					Thread.Sleep(10);
					i++;
				}

			} while (i < 3);
		}

		public Kolor WybierzKolor()
		{
			return (Kolor)rng.Next(1, 3);
		}

		public void WykonajRuch(Kolor kolor)
		{
			bool wynik = false;
			while (!wynik)
			{
				wynik = LosujPionek(kolor);
			}
		}

		private bool LosujPionek(Kolor kolor)
		{
			return plansza.UstawPionekNaPolu(rng.Next(0, 15), rng.Next(0, 15), kolor);
		}
	}
}
