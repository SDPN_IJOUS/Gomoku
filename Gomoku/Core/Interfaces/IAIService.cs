﻿using Gomoku.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gomoku.Core.Interfaces
{
	public interface IAIService
	{
		void UstawStartowePionki();

		Kolor WybierzKolor();

        void WykonajRuch(Kolor kolor);
	}
}
