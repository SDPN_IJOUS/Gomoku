﻿using Gomoku.Models.Enums;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;

namespace Gomoku.Core.Interfaces
{
	public interface IEngine
	{
		bool CzyGraWToku();

		void RozpocznijGre();

		void OnMouseClick(object sender, MouseButtonEventArgs e);

		void WybiezKolor(Kolor kolor);

		List<UIElement> ZwrocElementyInterfaceu();
	}
}