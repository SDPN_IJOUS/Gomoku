﻿using Gomoku.Models;
using Gomoku.Models.Enums;
using System.Collections.Generic;
using System.Windows;

namespace Gomoku.Core.Interfaces
{
	public interface IPlanszaService
	{
		PolePlanszyModel[,] Plansza { get; }

		List<UIElement> ZwrocElementyInterfaceu();

		bool UstawPionekNaPolu(int x, int y, Kolor kolor);

		bool SprawdzCzyZwyciezyl(Kolor kolor);

		void WyczyscPlansze();
	}
}