﻿using Gomoku.Models.Enums;
using System.Windows.Shapes;
using Gomoku.Core;
using System.Windows.Media;
using System.Windows.Controls;

namespace Gomoku.Models
{
	public class PolePlanszyModel
	{
		private Kolor kolor;
		public Kolor Kolor
		{
			get { return kolor; }
			set
			{
				kolor = value;
				UstawKolor(value);
			}
		}

		public Rectangle WidokPola { get; set; }
		public Ellipse WidokPionka { get; set; }

		public int X { get; private set; }
		public int Y { get; private set; }

		public PolePlanszyModel(int x, int y)
		{
			X = x * Ustawienia.Skala;
			Y = y * Ustawienia.Skala;
			InicjalizujWidoki();
			Kolor = Kolor.Brak;
		}

		private void InicjalizujWidoki()
		{
			WidokPola = new Rectangle() { Width = Ustawienia.Skala, Height = Ustawienia.Skala, Fill = Brushes.Bisque, Stroke = Brushes.Tan, StrokeThickness = 2 };
			Canvas.SetLeft(WidokPola, X);
			Canvas.SetTop(WidokPola, Y);
			Canvas.SetZIndex(WidokPola, 1);

			WidokPionka = new Ellipse() { Width = Ustawienia.Skala - 4, Height = Ustawienia.Skala - 4, Fill = Brushes.Transparent };
			Canvas.SetLeft(WidokPionka, X + 2);
			Canvas.SetTop(WidokPionka, Y + 2);
			Canvas.SetZIndex(WidokPionka, 2);
		}

		private void UstawKolor(Kolor value)
		{
			if (!WidokPionka.Dispatcher.CheckAccess())
			{
				WidokPionka.Dispatcher.Invoke(() => UstawKolor(value));
			}
			else
			{
				switch (value)
				{
					case Kolor.Brak:
						WidokPionka.Fill = Brushes.Transparent;
						break;
					case Kolor.Bialy:
						WidokPionka.Fill = Brushes.White;
						break;
					case Kolor.Czarny:
						WidokPionka.Fill = Brushes.Black;
						break;
				}
			}
		}
	}
}
