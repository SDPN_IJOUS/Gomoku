echo off
cls
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"
IF %ERRORLEVEL% EQU 0 (
    setlocal enableextensions enabledelayedexpansion
) ELSE (
	echo.
    ECHO Uruchom skrypt z podniesionymi uprawnieniami.
	echo.
    Pause
    EXIT /B 1
)

start powershell -NoProfile -ExecutionPolicy Bypass -File "D:\Repo\Gomoku\WystawianieWersji.ps1"