Temat 4: Kółko i krzyżyk do pięciu

● Zaimplementuj popularną grę w kółko i krzyżyk do 5 (inna nazwa to Gomoku). Więcej informacji na: https://pl.wikipedia.org/wiki/Gomoku

● Aktualizacja planszy jest dobrym kandydatem do testowania.

● Gramy przeciwko naiwnej sztucznej inteligencji (niemal losowi).